#!/usr/bin/env ruby
require 'net/http'
require 'net/smtp'

#
# Given a website and link, return the stock price
#
def getStockQuote(host, link)
    begin
        # Create a new HTTP connection
        httpCon = Net::HTTP.new(host, 80)

        # Perform a HEAD request
        resp = httpCon.get(link, nil)

        stroffset = resp.body =~ /smfield="price">/
        subset = resp.body.slice(stroffset+16, 10)
        limit = subset.index('<')
        return subset[0..limit-1]

    rescue NameError
        # Bad stock symbol, no stroffset
        $stderr.print "Request for " + ARGV[0].upcase + " failed. Sorry.\n"
        exit
    end
end

#
# Send a message to a user.
#
def sendStockAlert(user, msg)
    lmsg = ["Subject: Stock Alert\n", "\n", msg]

    begin
        Net::SMTP.start('localhost') do |smtp|
            smtp.sendmail(lmsg, "rubystockmonitor@localhost.localdomain", [user])
        end
    rescue
        puts "Connection to SMTP server failed. Sorry.\n"
        exit
    end
end

#
# Main program. Checks the stock within the price band every two
# minutes, emails and exits if the stock price strays from the band.
#
# Usage: ./stockprice.rb <symbol> [high low email_address]
#
begin
    # Checking script arguments
    unless ARGV.length >= 1
        puts "You need to provide a stock symbol in order to check it."
        puts "Usage: ./stockprice.rb <symbol> [high] [low] [email_address]\n"
        exit
    end

    if ARGV.length > 1
        unless ARGV.length == 4
            puts "You need to provide a max price, a min price and a mail address.\n"
            puts "Usage: ./stockprice.rb <symbol> [high low email_address]\n"
            exit
        end
        high = ARGV[1].to_f
        low = ARGV[2].to_f
        user = ARGV[3]
    end

    # Set URL
    host = "www.smartmoney.com"
    link = "/quote/" + ARGV[0].upcase + "/?story=snapshot&symbol=" + ARGV[0]

    # Main function
    while 1
        price = getStockQuote(host, link)
        print ARGV[0] + " current stock price is: " + price + " (from smartmoney.com)\n"
        price = price.to_f

        if ARGV.length == 4 then
            if (price > high) || (price < low) then
                if (price > high) then
                    msg = "Stock " + ARGV[0] + " has exceeded the price of " + high.to_s +
                        "\n" + host + link + "\n"
                else
                    msg = "Stock " + ARGV[0] + " has fallen below the price of " + low.to_s +
                        "\n" + host + link + "\n"
                end
                sendStockAlert(user, msg)
            end
        end

        sleep 120
    end
end

